$(function () {
    /* 訓練データの設定 */
    var RANGE = [20, 20];   //座標系の定義域幅
    var COLOR_T = 'rgb(255, 0, 0)';
    var COLOR_F = 'rgb(0, 0, 255)';
    (function () {
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
    })();

    var dataRef = {Data:null};
    var fileRef = {file:null};
    var dataCanvas = $('#dataCanvas')[0];
    var modelCanvas = $('#modelCanvas')[0];

    /* イベントハンドラの設定 */
    $('#dataCanvas').on('click', {dataRef:dataRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onCanvasClicked);
    $('#learn').on('click', {dataRef:dataRef, canvas:modelCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onTrain);
    $('#resetButton').on('click', {dataRef:dataRef, dataCanvas:dataCanvas, modelCanvas:modelCanvas}, onReset);
    $('#genFromFile').on('click', {dataRef:dataRef, fileRef:fileRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onGenerateFromFile);
    $('#download').on('click', {dataRef:dataRef}, onDownload);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        if($('input[name="clickLabel"]:eq(0)').prop('checked')) {
            $('input[name="clickLabel"]:eq(1)').prop('checked', true);
        } else {
            $('input[name="clickLabel"]:eq(0)').prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        console.log('Enter');
        $('#learn').trigger('click');
    }
}

function onCanvasClicked(event) {
    var canvas = event.data.canvas;
    var range = event.data.range;
    var canvasRect = canvas.getBoundingClientRect();
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;
    var cx = event.clientX - canvasRect.left;
    var cy = event.clientY - canvasRect.top;
    var c = [cx, cy];
    var x = canvasToData(c, canvas, range);
    var y = ($('input[name="clickLabel"]:checked').val() === "positive") ? 1: -1;
    var d = {x:x, y:y};

    if (event.data.dataRef.Data === null) {
        event.data.dataRef.Data = [];
    }
    event.data.dataRef.Data.push(d);
    drawPoint(d, canvas, range, color_t, color_f);
}

function onTrain(event) {
    alert('学習開始');
    var range = event.data.range;
    var canvas = event.data.canvas;
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;
    var itr = Number($('#itr').val());
    var maxDepth = Number($('#maxDepth').val());
    var m_l = new EnsembleModel();
    var l = new Adaboost(m_l, maxDepth, itr);
    l.train(event.data.dataRef.Data);

    out = $('#output span');
    out.children().remove();
    var htmlText = '<br />$N_{error} = ' + (l.check(event.data.dataRef.Data)) + '$<br />';
    for (var i=0; i < l.model.alphas.length; ++i) {
        htmlText += '$\\alpha _{' + i + '} = ' + numToString(l.model.alphas[i]) + '$';
        htmlText += '($error_{' + i + '} = ' + numToString(l.model.errors[i]) + '$)';
        if (i !== l.model.alphas.length - 1) {
            htmlText += ', ';
        }
    }
    out.append($('<sapn>').html(htmlText));

    MathJax.Hub.Queue(["Typeset", MathJax.Hub, out.id]);

    modelCanvas.getContext('2d').clearRect(0, 0, canvas.width, modelCanvas.height);
    drawDiscriminant(event.data.dataRef.Data, canvas, range, m_l, color_t, color_f);
}

function numToString(n) {
    var n_str;
    if (n >= 1 / 100) {
        n_str = n.toFixed(3);
    } else if (n === 0) {
        n_str = '0';
    } else {
        n_str = n.toExponential(3);
    }

    return n_str;
}

function onReset(event) {
    event.data.dataRef.Data = [];
    var dataCanvas = event.data.dataCanvas;
    var modelCanvas = event.data.modelCanvas;
    dataCanvas.getContext('2d').clearRect(0, 0, dataCanvas.width, dataCanvas.height);
    modelCanvas.getContext('2d').clearRect(0, 0, modelCanvas.width, modelCanvas.height);
    out = $('#output span');
    out.children().remove();
}
