var Adaboost = (function () {
    /* コンストラクタ */
    var Adaboost = function (m, maxDepth, i) {
        this.model = m; //学習モデル
        this.maxDepth = maxDepth;
        this.itr = i;   //学習回数
    }

    var pt = Adaboost.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var W = [];
        for (var i=0; i < Data.length; ++i) {
            W[i] = 1 / Data.length;
        }
        for (var i=0; i < this.itr; ++i) {
            if(!this.update(Data, W)) {
                break;
            }
        }
    }

    /* エラーチェック */
    pt.check = function (Data) {
        var error = 0;
        for (var i=0; i < Data.length; ++i) {
            if (this.model.judge(Data[i].x) !== Data[i].y) {
                error++;
            }
        }
        return error;
    }

    pt.update = function (Data, W) {
        var wl = new DecisionTree(2, average, W, this.maxDepth);
        wl.train(Data);
        var error = wl.error(Data, W);
        var alpha = 1 / 2 * Math.log((1 - error) / error);

        if (!(0 < error && error < 0.5)) {
            if (error === 0 && this.model.models.length === 0) {
                this.model.models.push(wl);
                this.model.alphas.push(alpha);
                this.model.errors.push(error);
            }
            return false;
        }
        this.model.models.push(wl);
        this.model.alphas.push(alpha);
        this.model.errors.push(error);

        this.updateW(Data, W, wl, alpha);
        return true;
    }

    pt.updateW = function (Data, W, wl, alpha) {
        for (var i=0; i < W.length; ++i) {
            W[i] *= Math.exp(-alpha * Data[i].y * wl.judge(Data[i].x));
        }
        var norm = 0;
        for (var i=0; i < W.length; ++i) {
            norm += W[i];
        }
        for (var i=0; i < W.length; ++i) {
            W[i] /= norm;
        }
    }

    return Adaboost;
})();
