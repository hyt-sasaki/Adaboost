var EnsembleModel = (function () {
    var EnsembleModel = function () {
        this.models = [];
        this.alphas = [];
        this.errors = [];
    }

    var pt = EnsembleModel.prototype;

    pt.sum = function (x) {
        var ret = 0;
        for (var i=0; i < this.models.length; ++i) {
            ret += this.alphas[i] * this.models[i].judge(x);
        }

        return ret;
    }

    pt.judge = function (x) {
        if (this.sum(x) >= 0) {
            return 1;
        } else {
            return -1;
        }
    }

    return EnsembleModel;
})();
