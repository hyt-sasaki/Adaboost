/* データの生成モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(a, w) {
        this.activate = a;
        this.w = w;     //重み
    }

    var pt = Model.prototype;

    pt.sum = function(x) {
        var ret = 0;
        for (var i=0; i < this.w.length; ++i) {
            ret += this.w[i] * phi(x, i);
        }
        return ret;
    }

    pt.out = function(x) {
        return this.activate(this.sum(x));
    }

    pt.judge = function (x) {
        if (this.sum(x) >= 0) {
            return 1;
        } else {
            return -1;
        }
    }

    return Model;
})();

function sigmoid(x) {
    return 1 / (1 + Math.exp(-x));
}

//function phi(x, i) {
//    var x_added = [1].concat(x);
//    return x_added[i];
//}

function phi(x, i) {
    var retArray = [1, x[0], x[1], x[0] * x[0], x[0] * x[1], x[1] * x[1]];
    //var retArray = [1, x[0], x[1], x[0] * x[0]];
    return retArray[i];
}
