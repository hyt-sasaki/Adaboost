var DecisionTree = (function () {
    /* コンストラクタ */
    var DecisionTree = function (classNum, devideFunc, W, maxDepth) {
        this.classNum = classNum;
        this.devideFunc = devideFunc;
        this.W = W;
        this.maxDepth = maxDepth;
        this.inverse = false;
    }

    var pt = DecisionTree.prototype;

    /* 学習 */
    pt.train = function (D) {
        this.root = new Node(D, this.classNum, this.devideFunc, this.W, 0, this.maxDepth);
        this.root.build();
    }

    pt.predict = function (x) {
        var ret = this.root.predict(x);
        return ret;
    }

    pt.judge = function (x) {
        var ret = this.root.predict(x);
        if (ret === 0) {
            ret = -1;
        }
        if (this.inverse) {
            if (ret === 1) {
                ret = -1;
            }
            if (ret === -1) {
                ret = 1;
            }
        }
        return ret;
    }

    pt.invert = function () {
        this.inverse = !this.inverse;
    }

    pt.check = function (Data) {
        var error = 0;
        for (var i=0; i < Data.length; ++i) {
            if (this.predict(Data[i].x) !== Data[i].y) {
                error ++;
            }
        }
        return error;
    }

    pt.error = function (Data, W) {
        var error = 0;
        for (var i=0; i < Data.length; ++i) {
            if (this.predict(Data[i].x) !== Data[i].y) {
                error += this.W[i];
            }
        }
        return error;
    }

    return DecisionTree;
})();

var Node = (function () {
    var Node = function (D, classNum, devideFunc, W, depth, maxDepth) {
        this.D = D;
        this.classNum = classNum;
        this.depth = depth;
        this.maxDepth= maxDepth;
        this.children = [];
        if (W === undefined) {
            this.W = [];
            for (var i=0; i < D.length; ++i) {
                this.W[i] = 1 / D.length;
            }
        } else {
            this.W = W;
        }
        this.calcThreshold = devideFunc;
        this.calcImpureness();
    }

    var pt = Node.prototype;

    pt.build = function () {
        var maxDeltaI = 0;
        var maxTheta = -1;
        var maxThreshold = 0;
        var bestChildren = [];

        if (this.D.length === 0) {
            return;
        }
        for (var theta=0; theta < this.D[0].x.length; ++theta) {
            var threshold = this.calcThreshold(this.D, theta, this.W);

            var LR = this.devideData(theta, threshold);
            var LRW = this.devideWeight(theta, threshold);
            var L = LR[0];
            var R = LR[1];
            var LW = LRW[0];
            var RW = LRW[1];
            var pL = 0;
            for (var i=0; i < LW.length; ++i) {
                pL += LW[i];
            }
            var pR = 0;
            for (var i=0; i < RW.length; ++i) {
                pR += RW[i];
            }
            normalize(LW);
            normalize(RW);
            var lNode = new Node(L, this.classNum, this.calcThreshold, LW, this.depth + 1, this.maxDepth);
            var rNode = new Node(R, this.classNum, this.calcThreshold, RW, this.depth + 1, this.maxDepth);

            var deltaI = this.impureness - (pL * lNode.impureness + pR * rNode.impureness);
            if (deltaI > maxDeltaI) {
                maxTheta = theta;
                maxDeltaI = deltaI;
                maxThreshold = threshold;
                bestChildren[0] = lNode;
                bestChildren[1] = rNode;
            }
        }
        if (maxTheta !== -1 && this.depth < this.maxDepth) {
            this.children = bestChildren;
            this.theta = maxTheta;
            this.threshold = maxThreshold;
            for (var i=0; i < this.children.length; ++i) {
                this.children[i].y = this.y + 1;
                this.children[i].build();
            }
        }
    }

    pt.calcImpureness = function () {
        var impureness = 1;
        var proportions = this.calcProportion();
        for (var i=0; i < proportions.length; ++i) {
            impureness -= Math.pow(proportions[i], 2);
        }
        this.impureness = impureness;
    }

    pt.calcProportion = function () {
        var dataNum = this.D.length;
        var proportion = [];
        for (var i=0; i < this.classNum; ++i) {
            proportion.push(0);
        }
        for (var i=0; i < dataNum; ++i) {
            proportion[this.D[i].y] += this.W[i];
        }

        return proportion;
    }

    pt.devideData = function (theta, threshold) {
        var L = [];
        var R = [];

        for (var i=0; i < this.D.length; ++i) {
            if (this.D[i].x[theta] < threshold) {
                L.push(this.D[i]);
            } else {
                R.push(this.D[i]);
            }
        }

        return [L, R];
    }

    pt.devideWeight = function (theta, threshold) {
        var LW = [];
        var RW = [];

        for (var i=0; i < this.D.length; ++i) {
            if (this.D[i].x[theta] < threshold) {
                LW.push(this.W[i]);
            } else {
                RW.push(this.W[i]);
            }
        }

        return [LW, RW];
    }

    pt.predict = function (x) {
        if (this.children.length !== 0) {
            var i;
            if (x[this.theta] < this.threshold) {
                i = 0;
            } else {
                i = 1;
            }
            return this.children[i].predict(x);
        } else {
            var ret = this.getMaxProportionClass();
            return ret;
        }
    }

    pt.getMaxProportionClass = function () {
        var proportions = this.calcProportion();
        var c = -1;
        var maxProportion = 0;
        for (var i=0; i < proportions.length; ++i) {
            if (proportions[i] > maxProportion) {
                c = i;
                maxProportion = proportions[i];
            }
        }
        return c;
    }

    return Node;
})();

function average(D, theta, W) {
    var mean = 0;
    for (var i=0; i < D.length; ++i) {
        mean += D[i].x[theta] * W[i];
    }

    return mean;
}

function median(D, theta, W) {
    var sorted = [];
    for (var i=0; i < D.length; ++i) {
        sorted.push(D[i].x[theta]);
    }
    sorted.sort(function (a, b) {
        if (a > b) {
            return 1;
        } else if ( a < b) {
            return -1;
        } else {
            return 0;
        }
    });
    var median = (sorted[0] + sorted[sorted.length - 1]) / 2;

    return median;
}

function normalize(W) {
    if (W.length !== 0) {
        var norm = 0;
        for (var i=0; i < W.length; ++i) {
            norm += W[i];
        }
        if (norm !== 0) {
            for (var i=0; i < W.length; ++i) {
                W[i] /= norm;
            }
        }
    }
}
